package main

import (
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/urko-b/bloxroute-project/cmd/logger"
	"github.com/urko-b/bloxroute-project/internal/item/infra/persistence"
	"github.com/urko-b/bloxroute-project/internal/item/use_case"
	"github.com/urko-b/bloxroute-project/pkg/rabbitmq"
	"log"
	"time"
)

var save_usecase *use_case.SaveUseCase
var get_usecase *use_case.GetUseCase
var get_all_usecase *use_case.GetUseAllCase
var remove_usecase *use_case.RemoveUseCase
var exchange_name = "bloXroute_exchange"

type bind struct {
	queue string
	key   string
}

func main() {
	lg := logger.NewLog("consumer")
	defer lg.Close()

	repository := persistence.NewItemRepository()

	save_usecase = use_case.NewSaveUseCase(repository)
	get_usecase = use_case.NewGetUseCase(repository)
	get_all_usecase = use_case.NewGetAllUseCase(repository)
	remove_usecase = use_case.NewRemoveUseCase(repository)

	ch, err := rabbitmq.OpenChannel()
	if err != nil {
		log.Fatalf("rabbitmq.OpenChannel: %s", err)
	}

	defer ch.Close()

	if err = ch.ExchangeDelete(exchange_name, false, false); err != nil {
		log.Fatalf("ch.ExchangeDelete: %s", err)
	}

	// See the Channel.Publish example for the complimentary declare.
	err = ch.ExchangeDeclare(exchange_name, "topic", true, false, false, false, nil)
	if err != nil {
		log.Fatalf("exchange.declare: %s", err)
	}

	bindings := []bind{
		{"item_save", "save"},
		{"item_get", "get"},
		{"item_get_all", "get_all"},
		{"item_remove", "remove"},
	}
	for _, b := range bindings {
		_, err = ch.QueueDeclare(b.queue, true, false, false, false, nil)
		if err != nil {
			log.Fatalf("queue.declare: %v", err)
		}

		if err = ch.QueueBind(b.queue, b.key, exchange_name, false, nil); err != nil {
			log.Fatalf("queue.bind: %v", err)
		}
	}

	err_chan := make(chan error)
	close_err_chan := func() {
		log.Printf("closing err_chan\n")
		err_chan <- nil
	}

	defer close_err_chan()
	go func() {
		for e := range err_chan {
			if e != nil {
				log.Printf("error: %s\n", e)
			}
		}
	}()

	log.Println("go consume_get")
	get_msgs, err := ch.Consume(
		"item_get",
		"item_get_consumer",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("get_msgs ch.Consume: %s", err)
	}
	go consume_get(get_msgs, err_chan)

	log.Println("go consume_save")
	save_msgs, err := ch.Consume(
		"item_save",
		"item_save_consumer",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("save_msgs ch.Consume: %s", err)
	}
	go consume_save(save_msgs, err_chan)

	log.Println("go consume_remove")
	remove_msgs, err := ch.Consume(
		"item_remove",
		"item-remove-consumer",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatalf("remove_msgs ch.Consume: %s", err)
	}
	go consume_remove(remove_msgs, err_chan)

	log.Println("go consume_get_all")
	get_all_msgs, err := ch.Consume(
		"item_get_all",
		"item_get_all_consumer",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		panic(fmt.Errorf("get_all_msgs ch.Consume: %s", err))
	}
	go consume_get_all(get_all_msgs)

	// Wait until you're ready to finish, could be a signal handler here.
	for true {
		time.Sleep(time.Second * 1)
	}
}

func consume_save(save_msgs <-chan amqp.Delivery, err_chan chan error) {
	var err error
	for msg := range save_msgs {

		var inputDto use_case.ItemInputDTO
		if err = json.Unmarshal(msg.Body, &inputDto); err != nil {
			err_chan <- fmt.Errorf("consume_save json.Unmarshal: %s", err)
		}
		log.Printf("item to save: %v\n", inputDto)

		if err = save_usecase.Execute(inputDto); err != nil {
			err_chan <- fmt.Errorf("save_usecase.Execute: %s", err)
		}
		if err = msg.Ack(false); err != nil {
			err_chan <- fmt.Errorf("save_usecase msg.Ack: %s", err)
		}
		log.Printf("saved item: %v\n", inputDto)
	}

}

func consume_get(get_msgs <-chan amqp.Delivery, err_chan chan error) {
	var err error
	inputDto := use_case.ItemInputDTO{}
	var item *use_case.ItemOutputDTO
	for msg := range get_msgs {
		if err = json.Unmarshal(msg.Body, &inputDto); err != nil {
			err_chan <- fmt.Errorf("consume_get json.Unmarshal: %s", err)
		}

		item, err = get_usecase.Execute(inputDto.ID)
		if err != nil {
			err_chan <- fmt.Errorf("get_usecase.Execute: %s", err)
		}
		if err = msg.Ack(false); err != nil {
			err_chan <- fmt.Errorf("get_usecase msg.Ack: %s", err)
		}
		log.Printf("get item: %v\n", item)
	}
}

func consume_get_all(get_msgs <-chan amqp.Delivery) {
	for msg := range get_msgs {
		items := get_all_usecase.Execute()
		msg.Ack(false)

		log.Printf("get all items: %d \n", len(items))
		for i, item := range items {
			log.Printf("item index at %d is %v\t", i, item)
		}

	}
}

func consume_remove(remove_mgs <-chan amqp.Delivery, err_chan chan error) {
	var err error
	inputDto := use_case.ItemInputDTO{}
	var item *use_case.ItemOutputDTO
	for msg := range remove_mgs {
		if err = json.Unmarshal(msg.Body, &inputDto); err != nil {
			err_chan <- fmt.Errorf("remove json.Unmarshal: %s", err)
		}

		if err = remove_usecase.Execute(inputDto.ID); err != nil {
			err_chan <- fmt.Errorf("remove_usecase.Execute: %s", err)
		}
		if err = msg.Ack(false); err != nil {
			err_chan <- fmt.Errorf("remove_usecase msg.Ack: %s", err)
		}
		log.Printf("remove item: %v\n", item)
	}
}
