package main

import (
	"context"
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/urko-b/bloxroute-project/cmd/logger"
	"github.com/urko-b/bloxroute-project/internal/item/use_case"
	"github.com/urko-b/bloxroute-project/pkg/rabbitmq"
	"log"
	"sync"
	"time"
)

var exchange_name = "bloXroute_exchange"

var routing_keys = []string{"save", "get", "remove", "get_all"}

func main() {
	lg := logger.NewLog("producer")
	defer lg.Close()

	ch, err := rabbitmq.OpenChannel()
	if err != nil {
		log.Fatalf("rabbitmq.OpenChannel: %s", err)
	}
	defer ch.Close()

	publish(context.Background(), ch, exchange_name)
}

func publish(ctx context.Context, ch *amqp.Channel, exchange_name string) {
	max_loop := 20

	err_chan := make(chan error)

	defer func() {
		err_chan <- nil
		log.Printf("closing err_chan\n")
		close(err_chan)
	}()
	go func() {
		for e := range err_chan {
			if e != nil {
				log.Printf("error: %s\n", e)
			}
		}
	}()

	wg := &sync.WaitGroup{}
	wg.Add(len(routing_keys) * max_loop)

	counter := 1
	mutex := sync.Mutex{}
	for i := 0; i < len(routing_keys); i++ {
		routing_key := routing_keys[i]
		for k := 0; k < max_loop; k++ {
			mutex.Lock()
			id := counter
			counter++
			mutex.Unlock()
			name := fmt.Sprintf("item %d", k)
			go produce(ctx, wg, ch, exchange_name, routing_key, name, id, err_chan)
		}
	}
	wg.Wait()
}

func produce(
	ctx context.Context,
	wg *sync.WaitGroup,
	ch *amqp.Channel,
	exchange_name, routing_key, name string,
	id int,
	error_ch chan error,
) {
	defer wg.Done()
	var bts []byte
	var err error

	switch routing_key {
	case "save", "get", "remove":
		body := use_case.ItemInputDTO{
			ID:   id,
			Name: name,
		}

		bts, err = json.Marshal(body)
		if err != nil {
			error_ch <- err
		}
		break
	case "get_all":
		bts = make([]byte, 0)
		break
	}

	msg := amqp.Publishing{
		Headers:      amqp.Table{},
		ContentType:  "text/plain",
		Body:         bts,
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
	}
	if err = ch.PublishWithContext(
		ctx,
		exchange_name,
		routing_key,
		false, // mandatory
		false, // immediate
		msg,
	); err != nil {
		error_ch <- fmt.Errorf("Exchange Publish error: %s\n", err)
	}

	log.Printf("Publish message: %s to %s\n", name, routing_key)
}
