package logger

import (
	"fmt"
	"log"
	"os"
	"time"
)

type Log struct {
	f *os.File
}

func NewLog(file_name string) *Log {
	//create your file with desired read/write permissions
	file_name = fmt.Sprintf("%s_%s.log", file_name, time.Now().Format("2006_01_02_15_04_05"))
	f, err := os.OpenFile(file_name, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}

	//set output of logs to f
	log.SetOutput(f)
	return &Log{}
}

func (l *Log) Close() {
	l.f.Close()
}
