
# Urko bloXRoute exercise

For this project I've been using examples of official documentation for golang
package of [rabbitmq](https://pkg.go.dev/github.com/rabbitmq/amqp091-go@v1.5.0#section-readme)

Also I took some ideas from this github repo, just to clarify my ideas about [hexagonal/onion architecture](https://github.com/MarioCarrion/todo-api-microservice-example/tree/b886f4346851c860728ec7924d8b3196139af11e/cmd)

I've been trying to follow SOLID principles and Hexagonal/Onion architecture: 

I've followed go official instructions to handle maps in iteration and concurrency: https://go.dev/blog/maps

I've been handling some minor concurrency problems with go mutex Lock() and Unlock(): https://go.dev/tour/concurrency/9

## Internal
### internal > item > entiy
This package contains the domain definition in this case for our `item` in the file named `item.go`
The file `item_iface.go` contains the interface that will handle our domain actions

### internal > item > infra > persistence
Here we can find the implementation of our domain logic. We could create another persistence method
with database queries but now we have used in memory persistence. 

### internal > use_case
I've decided to place all the business logic from our domain in separated `go` files. One for each
use case. Inside each file we can find the abstraction of the but  

## pkg > rabbitmq
This package contains the connection with rabbitmq server deployed by docker-compose

## cmd > consume
Here we have our consumer: is the server who is gonna consume the messages from our producer (client)

## cmd > producer
Will produce a lot of messages to our rabbit queues in concurrent mode.

# How to run

## Requirements
### Docker
First of all is needed to have docker installed. Go to official [docker-compose](https://docs.docker.com/compose/install/) installed
### Go latest stable version
Please download Go latest version from official [website](https://go.dev/dl/) 


After you install it open a command line tab or window and run
`docker-compose up`
This will open you a rabbitmq server instance in a docker container

## Consume
We need our server to be up to consume messages produced by our producer(client). So open new console window or tab
and then:
```
cd cmd/consume
go run main.go
```

## Producer
To start producing messages that will be handled by our consumer (server) then we need to run these commands.
First go to directory and run `main.go` file
```
cd cmd/producer
go run main.go
```

I didn't have time to prepare a docker image that will bundle the whole application but these steps
are working on a local linux machine.