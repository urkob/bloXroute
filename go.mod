module github.com/urko-b/bloxroute-project

go 1.19

require (
	github.com/rabbitmq/amqp091-go v1.5.0
	golang.org/x/exp v0.0.0-20221204150635-6dcec336b2bb
)
