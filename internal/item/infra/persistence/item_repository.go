package persistence

import (
	"errors"
	"fmt"
	"github.com/urko-b/bloxroute-project/internal/item/entity"
	"golang.org/x/exp/slices"
	"sync"
)

// ItemRepository This could be some database connection instead of our in memory array.
type ItemRepository struct {
	sync.RWMutex
	items map[int]*entity.Item
	keys  []int
}

func NewItemRepository() *ItemRepository {
	return &ItemRepository{
		items: map[int]*entity.Item{},
		keys:  make([]int, 0),
	}
}

func (ir *ItemRepository) Add(item *entity.Item) error {
	it := ir.get_item(item.ID)
	if it != nil {
		return errors.New("Item already get_item")
	}
	ir.add_item(item)
	return nil
}

// add_item related to go docs https://go.dev/blog/maps
// map and concurrency are not safe by default.
func (ir *ItemRepository) add_item(item *entity.Item) {
	ir.Lock()
	ir.items[item.ID] = item
	ir.Unlock()
	ir.keys = append(ir.keys, item.ID)
}

func (ir *ItemRepository) Get(id int) (*entity.Item, error) {
	it := ir.get_item(id)
	if it == nil {
		return nil, fmt.Errorf("Item with ID: %d was not found", id)
	}

	return it, nil
}

func (ir *ItemRepository) GetAll() []entity.Item {
	return ir.get_all_sorted()
}

func (ir *ItemRepository) Remove(id int) error {
	it := ir.get_item(id)
	if it == nil {
		return fmt.Errorf("Item with id %d doesn't exist", id)
	}
	ir.remove_item(id)
	return nil
}

func (ir *ItemRepository) remove_item(id int) {
	ir.Lock()
	delete(ir.items, id)
	ir.Unlock()
	ir.keys = slices.Delete(ir.keys, id-1, id)
}

func (ir *ItemRepository) get_all_sorted() []entity.Item {
	items := make([]entity.Item, 0, len(ir.items))
	for _, k := range ir.keys {
		it := ir.get_item(k)
		items = append(items, *it)
	}

	return items
}

func (ir *ItemRepository) get_item(id int) *entity.Item {
	ir.RLock()
	it := ir.items[id]
	ir.RUnlock()
	return it
}
