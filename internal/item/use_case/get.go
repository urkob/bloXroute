package use_case

import "github.com/urko-b/bloxroute-project/internal/item/entity"

type GetUseCase struct {
	ItemRepository entity.ItemRepositoryIface
}

func NewGetUseCase(itemRepository entity.ItemRepositoryIface) *GetUseCase {
	return &GetUseCase{
		ItemRepository: itemRepository,
	}
}

func (s *GetUseCase) Execute(id int) (*ItemOutputDTO, error) {
	item, err := s.ItemRepository.Get(id)
	if err != nil {
		return nil, err
	}

	return &ItemOutputDTO{
		ID:   item.ID,
		Name: item.Name,
	}, nil
}
