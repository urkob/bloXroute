package use_case

import "github.com/urko-b/bloxroute-project/internal/item/entity"

type RemoveUseCase struct {
	ItemRepository entity.ItemRepositoryIface
}

func NewRemoveUseCase(itemRepository entity.ItemRepositoryIface) *RemoveUseCase {
	return &RemoveUseCase{
		ItemRepository: itemRepository,
	}
}

func (s *RemoveUseCase) Execute(id int) error {
	if err := s.ItemRepository.Remove(id); err != nil {
		return err
	}

	return nil
}
