package use_case

import (
	"github.com/urko-b/bloxroute-project/internal/item/entity"
)

type GetUseAllCase struct {
	ItemRepository entity.ItemRepositoryIface
}

func NewGetAllUseCase(itemRepository entity.ItemRepositoryIface) *GetUseAllCase {
	return &GetUseAllCase{
		ItemRepository: itemRepository,
	}
}

func (s *GetUseAllCase) Execute() []ItemOutputDTO {
	items := s.ItemRepository.GetAll()
	var output_items = make([]ItemOutputDTO, 0)

	for _, i := range items {
		output_items = append(output_items, ItemOutputDTO{
			ID:   i.ID,
			Name: i.Name,
		})
	}
	return output_items
}
