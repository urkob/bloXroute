package use_case

import "github.com/urko-b/bloxroute-project/internal/item/entity"

type ItemInputDTO struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type ItemOutputDTO struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type SaveUseCase struct {
	ItemRepository entity.ItemRepositoryIface
}

func NewSaveUseCase(itemRepository entity.ItemRepositoryIface) *SaveUseCase {
	return &SaveUseCase{
		ItemRepository: itemRepository,
	}
}

func (s *SaveUseCase) Execute(dto ItemInputDTO) error {
	item, err := entity.NewItem(dto.ID, dto.Name)
	if err != nil {
		return err
	}

	if err = s.ItemRepository.Add(item); err != nil {
		return err
	}

	return nil
}
