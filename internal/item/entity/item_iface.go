package entity

type ItemRepositoryIface interface {
	Add(item *Item) error
	GetAll() []Item
	Get(id int) (*Item, error)
	Remove(id int) error
}
