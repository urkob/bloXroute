package entity

import "errors"

type Item struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (i Item) IsValid() error {
	if i.ID <= 0 {
		return errors.New("ID cannot lower than 0")
	}

	if i.Name == "" {
		return errors.New("Name cannot be empty")
	}

	return nil
}

func NewItem(id int, name string) (*Item, error) {
	item := &Item{
		ID:   id,
		Name: name,
	}
	if err := item.IsValid(); err != nil {
		return nil, err
	}

	return item, nil
}
